# Kryptex

A tiny cryptography program I made during my studies.

![Kryptex](KrypTex.png)

## Functionality

- rndKey - generates a random Key based on the requirements of the selected algorithm
- Encrypt - encrypt message with given key
- Decrypt - decrypt message with given key
- Save - save a file(.txt) with the cipher on the local drive
- Load - load save a cipher from the local drive
- Quit - quits the program obvs.

## Encryption Algorithims

- AES
- MD5
- SHA-1
- SHA-512
- Viginere

## Dependecies

### JDK

java-11-openjdk

### Libraries

- java.awt
- java.io
- java.nio
- java.math
- java.security
- javax.swing
- java.util

## Use JAR

```
    java -jar KrypTex.jar
```
