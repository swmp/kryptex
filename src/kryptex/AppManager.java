package kryptex;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import encryptions.*;

public class AppManager {
	
	private static AppManager INSTANCE = new AppManager();
	private GUI gui;
	
	
	private IFEncryption modus;
	private IFEncryption[] allModi;
	private IFEncryption viginere;
	private IFEncryption md5;
	private IFEncryption sha1;
	private IFEncryption sha512;
	private IFEncryption aes;
	
	private String input;
	private String output;
	private String key;
	private String[] comboList = {"Viginere", "MD5","SHA1","SHA-512","AES"};
	
	
	
	/*
	 * @return statische AppManager instanz
	 */
	public static AppManager run() {
		return INSTANCE;
	}
	
	
	/*
	 * Konstruktor erzeugt Objekte der jeweiligen Verschluesselungen
	 * und instanziiert die GUI
	 * 
	 * 
	 */
	private AppManager() 
	{
		INSTANCE = this;
		
		/*
		 * hier sollen im weiteren verlauf klassen als module aus einem ordner geladen werden
		 * um das erweitern der funktion zu vereinfachen
		 */
		viginere = new Viginere();
		md5 = new MD5();
		sha1 = new SHA1();
		sha512 = new SHA512();
		aes = new AES();
		
		
		allModi = new IFEncryption[] {viginere,md5,sha1,sha512,aes};
		modus = allModi[0];
		
		gui = new GUI();
	}
	
	/*
	 * Aktualisiere den AppManager
	 * werte werden aus den Textfeldern geholt und in die klassenAttribute uebertragen
	 */
	public void refresh() 
	{
		input = gui.getInput();
		key = gui.getKey();
		output = gui.getOutput();
	}
	
	/*
	 * Verschluesselungsfunktion
	 * AppManager wird aktualisiert, der Text verschlüsselt, GUI Aktualisiert
	 */
	public void encrypt() 
	{
		refresh();
		output = modus.encrypt(input, key);
		gui.refresh();
	}
	
	/*
	 * Entschluesselungsfunktion
	 * AmppManager wird aktualisiert, der Text entschlüsselt, GUI Aktualisiert
	 */
	public void decrypt() 
	{
		refresh();
		output = modus.decrypt(input, key);
		gui.refresh();
	}
	
	/*
	 * Funktion zum Laden einer .txt Datei
	 */
	public void loadFile() 
	{
		
		StringBuilder sb;
		Scanner filescanner;
		
		//datei waehlen anzeige filtert txt dateien
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileFilter(new FileNameExtensionFilter(".txt","txt"));
		
		//speicher den returnwert
		int retVal = fileChooser.showOpenDialog(null);
		
		//wennn datei erfolgreich ausgewaehlt
		if(retVal == JFileChooser.APPROVE_OPTION) 
		{
			sb = new StringBuilder();
			
			//try faengt fehler ab
			try {
				filescanner = new Scanner(fileChooser.getSelectedFile());
				
				//so lange es weitere zeilen gibt
				while(filescanner.hasNextLine()==true)
				{
					//füge die naechste zeile hinzu
					sb.append(filescanner.nextLine()+'\n');
				}
				
				filescanner.close();
				
				//lade den geladenen text in die gui
				gui.setInput(sb.toString());  
				//lade den dateinamen als schluessel
				gui.setKey(fileChooser.getSelectedFile().getName());
				
			} catch (Exception e) 
			{
				gui.setOutput("error");
			}
			
		} 
		
		refresh();
		gui.refresh();
		
	}
	
	/*
	 * Funktion zum Speichern einer Datei
	 * setzt den Schlüssel als Dateinamen
	 * 
	 * bei der ausgabe wird auf zeilenumbrueche verzichtet
	 * diese sollen spaeter über eine filtermaske mitgelesen werden werden
	 * 
	 */
	public void saveFile() 
	{
		
		JFileChooser fileChooser = new JFileChooser();
		
		/*
		 * dei funktion empfielt das speichern als txt jedoch
		 * mus nach dem laden einer viginereverschluesselung so die txt endung vor dem
		 * entschluesseln entfernt werden
		 * 
		 */
		fileChooser.setFileFilter(new FileNameExtensionFilter(".txt","txt"));
		fileChooser.setSelectedFile(new File(getKey()+".txt"));
		
		int retVal = fileChooser.showSaveDialog(null);
		
		if(retVal == JFileChooser.APPROVE_OPTION) 
		{
			
			try 
			{
				PrintWriter pw = new PrintWriter(fileChooser.getSelectedFile());
				pw.write(output);
				pw.close();
			
			} catch (IOException e) 
			{
				System.out.println("error");
			}
		}
		
		refresh();
		
	}
	
	/*
	 * Funktion zum Beenden des Programms
	 */
	public static void quit() 
	{
		System.exit(0);
	}
	
	
	/*
	 * 
	 * Getter/Setter
	 * 
	 */
	public IFEncryption getModus() 
	{
		return modus;
	}
	public String getInput() 
	{
		return input;
	}
	public String getOutput() 
	{
		return output;
	}
	public IFEncryption[] getMods() 
	{
		return allModi;
	}
	public String[] getCombolist() 
	{
		return comboList;
	}
 	public String getKey() 
	{
		return key;
	}
	public void setRandomKey() 
	{
		key = modus.getRndKey();
	}
	public void setOutput(String out) 
	{
		output = out;
	}
	public void setModus(IFEncryption mod) 
	{
		modus = mod;
	}
	
	
}
