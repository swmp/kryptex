package kryptex;


import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;

import java.awt.Color;
import java.awt.Font;


public class Button extends JButton{
	
	/*
	 *Button variablen 
	 */
	private static final long serialVersionUID = 1L;
	private Icon icon = new ImageIcon(getClass().getResource("/media/button.png"));
	private Icon icon_h = new ImageIcon(getClass().getResource("/media/button_h.png"));
	private Icon sicon = new ImageIcon(getClass().getResource("/media/sbutton.png"));
	private Icon sicon_h = new ImageIcon(getClass().getResource("/media/sbutton_h.png"));
	private Color fontColor = GUI.ORANGE;
	private Color fontHover = GUI.WHITE;
	private Font font = GUI.sFont;
	
	public Button(String text) 
	{
		super(text);
		
		
		this.setIcon(icon);
		this.setBorder(null);
		this.setBorderPainted(false);
		this.setContentAreaFilled(false);
		this.setHorizontalTextPosition(JButton.CENTER);
		this.setVerticalTextPosition(JButton.CENTER);
		this.setForeground(fontColor);
		this.setFont(font);
		
		/*
		 * mouselistener laesst die buttons beim ueberfliegen "aufleuchten"
		 * dies waere auch ueber den swing toolkit gegangen aber nach etwas ueberlegen
		 * schien mir langfristig die bessere wahl
		 */
		
		this.addMouseListener(new java.awt.event.MouseAdapter() {
		    public void mouseEntered(java.awt.event.MouseEvent evt) {
		        setIcon(icon_h);
		        setForeground(fontHover);
		    }

		    public void mouseExited(java.awt.event.MouseEvent evt) {
		    	setIcon(icon);
		    	setForeground(fontColor);
		    }
		});
		
		
	}
	
	
	/*
	 * wird der Konstruktor mit einem boolean aufgeruf, so wird ein 
	 * kleiner Button erzeugt
	 */
	public Button(String text, Boolean b) 
	{
		super(text);
		
		
		this.setIcon(sicon);
	
		this.setContentAreaFilled(false);
		this.setHorizontalTextPosition(JButton.CENTER);
		this.setVerticalTextPosition(JButton.CENTER);
		this.setForeground(fontColor);
		this.setBorderPainted(false);
		
		this.addMouseListener(new java.awt.event.MouseAdapter() {
		    public void mouseEntered(java.awt.event.MouseEvent evt) {
		        setIcon(sicon_h);
		        
		    }

		    public void mouseExited(java.awt.event.MouseEvent evt) {
		    	setIcon(sicon);
		    	
		    }
		});
	}
}
