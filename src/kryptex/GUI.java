

package kryptex;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;


import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

class GUI extends JFrame{
	
	
	/**
	 * Variablen zur besseren Übersicht zusammengefasst deklariert
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane, pnLeft,pnCenter,pnRight,pnKeyArea,
					pnMenu,pnButtons,pnTitle,pnRND;
	
	private JComboBox<String> btnSelect;
	private JLabel lblInfo;
	private JTextField tfEncryptionKey;
	private JTextArea taInput, taOutput;
	//private JCheckBox cbSpace;
	private Dimension dimWindowSize = new Dimension(800,600);
	
	
	
	static Color BLACK = new Color(33,33,33);
	static Color WHITE = new Color(242,245,244);
	static Color GREY = new Color(42,46,49);
	static Color YELLOW = new Color(247,197,22);
	static Color ORANGE = new Color(238,77,46);	
	static Color LIGREY = new Color(69,69,66);
	static Font font = new Font("Helvetica",Font.PLAIN, 23);
	static Font sFont = new Font("Helvetica",Font.PLAIN, 15);
	
	/*
	 * 
	 * 
	 * Konstruktor ruft JFrame super() auf und startet die Build-routine
	 */
	GUI()
	{
		
		super("KrypTex");
		
		init();//erzeuge Fenster
		createPanels();//erzeuge Panels
		createGuiElem();//fuegt gui elemente hinzu
		
		this.setVisible(true);
	}
	
	/*
	 * 
	 * 
	 * Initialisiert das Hauptfenster
	 */
	private void init() 

	{
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//starte das fenster in der mitte des display in 800x600
		setSize(new Dimension(dimWindowSize));
		setLocationRelativeTo(null);
		setMinimumSize(new Dimension(dimWindowSize));
		
		//Border und Layout
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(2, 2, 2, 2));
		
		contentPane.setBackground(BLACK);
		contentPane.setLayout(new GridLayout(1,3));
		 
		setContentPane(contentPane);
	}
	
	/*
	 * Update Guielemente
	 * bei einem Update der GUI werde die Werte aus dem AppManager 
	 * ind die GUI geladen und die fenster mit repaint() aktualisiert
	 * 
	 * - bei einem Update des AppManagers laeuft es genau anders rum
	 * 
	 */
	public void refresh() 
	{	
		
		tfEncryptionKey.setText(AppManager.run().getKey());
		tfEncryptionKey.repaint();
		
		taInput.setText(AppManager.run().getInput());
		taInput.repaint();
		
		taOutput.setText(AppManager.run().getOutput());
		taOutput.repaint();
	}
	
	/*
	 * Erzeuge Panele
	 * Die Panels werden schrittweise von unten nach oben und 
	 * von links nach rechts aufgebaut
	 */
	private void createPanels() 
	{
		pnLeft = new JPanel();
		pnLeft.setBorder(new EmptyBorder(2,2,2,2));
		pnLeft.setBackground(null);
		pnLeft.setLayout(new BorderLayout());
		contentPane.add(pnLeft);
		
		pnCenter = new JPanel();
		pnCenter.setBorder(new EmptyBorder(2,2,2,2));
		pnCenter.setBackground(LIGREY);
		pnCenter.setLayout(new BorderLayout());
		contentPane.add(pnCenter);
		
		pnRight = new JPanel();
		pnRight.setBorder(new EmptyBorder(2,2,2,2));
		pnRight.setBackground(null);
		pnRight.setLayout(new BorderLayout());
		contentPane.add(pnRight);

		pnKeyArea = new JPanel();
		pnKeyArea.setBackground(null);
		pnKeyArea.setLayout(new BorderLayout());
		pnCenter.add(pnKeyArea,BorderLayout.NORTH);
		
		pnMenu = new JPanel();
		pnMenu.setBackground(null);
		pnMenu.setAlignmentX(CENTER_ALIGNMENT);
		pnCenter.add(pnMenu,BorderLayout.CENTER);
		
		pnTitle = new JPanel();
		pnTitle.setBackground(null);
		pnTitle.setAlignmentX(CENTER_ALIGNMENT);
		
		pnCenter.add(pnTitle,BorderLayout.SOUTH);
		
		pnRND = new JPanel();
		pnRND.setBackground(null);
		pnRND.setLayout(new BoxLayout(pnRND, BoxLayout.Y_AXIS));
		pnKeyArea.add(pnRND, BorderLayout.CENTER);
		
		pnButtons = new JPanel();
		pnButtons.setBackground(null);
		pnButtons.setLayout(new GridLayout(5,1));
		pnMenu.add(pnButtons);
		
		
		
		
		
	}
	
	
	/*
	 * 
	 * 
	 * Fuege GUI Elemente hinzu
	 */
	private void createGuiElem() 
	{
		
		addLabels();//Labels und Titel
		addSelect();//jCombobox zur auswahl der verschluesselungen
		addTextfields();//Textfelder 
		addButtons();//buttons
		
	}
	
	/*
	 * 
	 * 
	 * Labels
	 */
	private void addLabels() {
		
		lblInfo = new JLabel("<html><center><br>+-+-+-+-+-+-+-+<br>" + 
				"|K|r|y|p|T|e|x|<br>" + 
				"+-+-+-+-+-+-+-+</center><br></html>");
		lblInfo.setForeground(ORANGE);
		lblInfo.setAlignmentX(CENTER_ALIGNMENT);
		lblInfo.setFont(font);
		pnTitle.add(lblInfo);
	}
	
	/*
	 * Hinzufuegen der Buttons
	 * Erzeugt Objekte vom Typ Button
	 * Konstruktoraufruf mit einem boolean als argument erzeugt einen kleinen button
	 * 
	 */
	private void addButtons() {
		
		
		/*
		 * Encrypt Button
		 */
		Button btnEncyrpt = new Button("ENCRYPT");
		btnEncyrpt.addActionListener(new ActionListener() 
		{
			
			@Override //Verschlüssel den Input
			public void actionPerformed(ActionEvent arg0) 
			{
				AppManager.run().encrypt();
				
			}
		});
		pnButtons.add(btnEncyrpt);
		
		/*
		 * Decrypt Button
		 */
		Button btnDecrypt = new Button("DECRYPT");
		btnDecrypt.addActionListener(new ActionListener() 
		{
			
			@Override //Entschlüssel den Input 
			public void actionPerformed(ActionEvent arg0) 
			{
				AppManager.run().decrypt();
				
			}
		});
		pnButtons.add(btnDecrypt);
		
		/*
		 * Save Button
		 */
		Button btnSave = new Button("SAVE");
		btnSave.addActionListener(new ActionListener() 
		{
			
			@Override //Speichere den Output als .txt Datei
			public void actionPerformed(ActionEvent arg0) 
			{
				AppManager.run().saveFile();
				
			}
		});
		pnButtons.add(btnSave);
		
		/*
		 * Load Button
		 */
		Button btnLoad = new Button("LOAD");
		btnLoad.addActionListener(new ActionListener() 
		{
			
			@Override //Lade eine .txt Datei
			public void actionPerformed(ActionEvent arg0) 
			{
				AppManager.run().loadFile();
				refresh();
			}
			
		});
		pnButtons.add(btnLoad);
		
		/*
		 * Quit Button
		 */
		Button btnQuit = new Button("QUIT");
	
		btnQuit.addActionListener(new ActionListener()
		{
			
			@Override //Beende das Programm
			public void actionPerformed(ActionEvent arg0) 
			{
				AppManager.quit();
			}
		});
		pnButtons.add(btnQuit);
		
		/*
		 * Random Key Button
		 */
		Button btnRndkey = new Button("rndKey",true);
		btnRndkey.setAlignmentX(CENTER_ALIGNMENT);
		btnRndkey.addActionListener(new ActionListener() {
			
			@Override //Erzeuge einen zufaelligen Schluessel
			public void actionPerformed(ActionEvent arg0) 
			{
				tfEncryptionKey.setText(AppManager.run().getModus().getRndKey());
				AppManager.run().refresh();
				refresh();
			}
		});
		
		pnRND.add(btnRndkey,BorderLayout.SOUTH);
	}
	
	/*
	 * Hinzufuegen der Textfelder
	 */
	private void addTextfields() 
	{	
		
		
		//Eingabe Textfeld
		taInput = new JTextArea();
		taInput.setWrapStyleWord(true);
		taInput.setTabSize(4);
		taInput.setLineWrap(true);
		taInput.setBackground(GREY);
		taInput.setFont(font);
		taInput.setForeground(WHITE);
		taInput.setText("Plaintext");
		taInput.setAlignmentY(CENTER_ALIGNMENT);
		pnLeft.add(taInput,BorderLayout.CENTER);
		pnLeft.add(new JScrollPane (taInput));

		
		//Ausgabe Textfeld
		taOutput = new JTextArea();
		taOutput.setWrapStyleWord(true);
		taOutput.setTabSize(4);
		taOutput.setLineWrap(true);
		taOutput.setBackground(GREY);
		taOutput.setFont(font);
		taOutput.setForeground(WHITE);
		taOutput.setText("Cypher");
		taOutput.setAlignmentX(CENTER_ALIGNMENT);
		taOutput.setAlignmentY(CENTER_ALIGNMENT);
		taOutput.setEditable(false);
		pnRight.add(taOutput,BorderLayout.CENTER);
		pnRight.add(add(new JScrollPane (taOutput)));
		
		//Schluessel Textfeld
		tfEncryptionKey = new JTextField();
		tfEncryptionKey.setColumns(17);
		tfEncryptionKey.setText(AppManager.run().getModus().getRndKey());
		tfEncryptionKey.setBackground(GREY);
		tfEncryptionKey.setForeground(YELLOW);
		tfEncryptionKey.setFont(sFont);
		tfEncryptionKey.setHorizontalAlignment(JTextField.CENTER);
		pnRND.add(tfEncryptionKey,BorderLayout.CENTER);
	}
	
	/*
	 * Selectemenu
	 * 
	 */
	
	private void addSelect()
	{
		
		btnSelect = new JComboBox<>(AppManager.run().getCombolist());
		btnSelect.setSize(20, 10);
		btnSelect.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent arg0) {
				
				/*
				 * der Index des ausgewaehlten Strings wird als adresse
				 * genommen fuer ein Array mit den jeweiligen Verschluesselungen
				 * und der return Wert davon als neuer Modus definiert
				 * danach AppManager und gui aktualisiert
				 * 
				 */
				AppManager.run().setModus(AppManager.run().getMods()[btnSelect.getSelectedIndex()]);
				AppManager.run().refresh();
				refresh();
			}
		});
		pnKeyArea.add(btnSelect,BorderLayout.NORTH);
	}
	
	/*
	private void addCheckBox() 
	{
		cbSpace = new JCheckBox("ignore spaces");
		pnKeyArea.add(cbSpace,BorderLayout.SOUTH);
	}
	*/
	
	/*
	 * 
	 * 
	 * Getter/Setter
	 */
	public String getInput() 
	{
		return taInput.getText();
	}
	public String getKey() {
		return tfEncryptionKey.getText();
	}
	public String getOutput() {
		return taOutput.getText();
	}
	public void setKey(String in) {
		tfEncryptionKey.setText(in);
	}
	public void setInput(String input) {
		taInput.setText(input);
	}
	public void setOutput(String output) {
		taOutput.setText(output);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}