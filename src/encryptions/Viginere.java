package encryptions;

import java.util.Random;
/*
 * Verschluesselungsmodul für die Viginere-Verschluesselung
 */
public final class Viginere implements IFEncryption
{
	private String name = "Viginere";
	
	/*
	 * 
	 * Verschluesselungsfunktion
	 * 
	 */
	String err = "Fehler";
	
	public String encrypt(String plainText, String key) 
	{
		StringBuilder sb = new StringBuilder();
		//Eingangsstring wird zu Kleinbuchstaben gecastet
		String text = plainText.toLowerCase();
		
		//String bis zum Ende durchgehen
		for(int i = 0, j = 0; i < text.length(); i += 1, j += 1) {
			
			/*
			 * wenn der Char an i nicht im Bereich vo 'a' - 'z'
			 * liegt dann wird j um einen zurück gesetzt
			 * dadurch werden Sonder und Leerzeichen einfach ignoriert
			 * 
			 * im weiteren verlauf sollen Sonder wie auch Leerzeichen über
			 * einen Flag eine sonderbehandlung bekommen
			 */
			if(text.charAt(i) < 97 || text.charAt(i) > 122){
				j -= 1;
				continue;
			}else if(j == key.length()) { 
				//bei Ende des Codewods wird der Zaheler zurueckgesetzt
				j = 0;
			}

			char digest;
			
			try {
				/* 
				 * offset 'a' = 97
				 * offset alphabet = 26
				 * 
				 * zb. 
				 * 
				 * 'd' + 'i' = 100 + 105
				 * (100+105)%97 = 11
				 * 11%26 = 11
				 * 11 + 97 = 108 = 'l'
				 * 
				 */
				digest = (char) ((((text.charAt(i)+key.charAt(j))%97)%26)+97);

			} catch (StringIndexOutOfBoundsException e) 
			{
				return err;
			}
			sb.append(digest);
		}
		
		return sb.toString();
	}
	
	/*
	 * 
	 * Entschluesselungsfunktion
	 * 
	 * 
	 */
	public  String decrypt(String cypherText, String key) 
	{
		StringBuilder sb = new StringBuilder();
		String text = cypherText;
		
		for(int i = 0, j = 0; i < text.length(); i += 1, j += 1) 
		{
			if(text.charAt(i) < 97 || text.charAt(i) > 122)
			{
				j -= 1;
				continue;
			}else if(j == key.length()) 
			{
				j = 0;
			}
			
			char digest;
			
			try 
			{
				digest = (char) ((((text.charAt(i)-key.charAt(j)+26)%97)%26)+97);

			} catch (Exception e) 
			{
				return err;
			}
			
			sb.append(digest);
		}
		
		return sb.toString();
	}
	
	/*
	 * 
	 * Erzeugt einen zufaelligen Schluessel der laenge keySize
	 * schluessel besteht aus den Chars 'a' - 'z'
	 */
	public String getRndKey() 

	{	int keySize = 23;
		StringBuilder sb = new StringBuilder();
		Random rng = new Random();
		
		for(int i = 0; i < keySize; i += 1) 
		{
			sb.append((char)(rng.nextInt(25)+97));
		}
		return sb.toString();
	}
	
	public String getName() {
		return name;
	}

	
	public String encrypt(String plainText) {
		
		return err;
	}

	
	public String decrypt(String cypherText) {
		decrypt(cypherText,null);
		return err;
	}
}
