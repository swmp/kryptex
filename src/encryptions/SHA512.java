package encryptions;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

public class SHA512 implements IFEncryption{

	@Override
	public String encrypt(String plainText) {
		
		return encrypt(plainText,null);
	}

	@Override
	public String encrypt(String plainText, String key) {
		String generatedPassword = null;
	    try {
	        MessageDigest md = MessageDigest.getInstance("SHA-512");
	        md.update(key.getBytes(StandardCharsets.UTF_8));
	        byte[] bytes = md.digest(plainText.getBytes(StandardCharsets.UTF_8));
	        StringBuilder sb = new StringBuilder();
	        for(int i=0; i< bytes.length ;i++){
	            sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
	        }
	        generatedPassword = sb.toString();
	    } catch (NoSuchAlgorithmException e) {
	        e.printStackTrace();
	    }
	    return generatedPassword;
	}

	

	@Override
	public String decrypt(String cypherText, String key) {
	
		return "hashes are one-way functions!";
	}

	@Override
	public String getRndKey() {
		int keySize = 23;
		StringBuilder sb = new StringBuilder();
		Random rng = new Random();
		
		for(int i = 0; i < keySize; i += 1) 
		{
			sb.append((char)(rng.nextInt(127)+32));
		}
		return sb.toString();
	}
}
