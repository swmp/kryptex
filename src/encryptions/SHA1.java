package encryptions;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

public class SHA1 implements IFEncryption{

	@Override
	public String encrypt(String plainText, String key) {
		try { 
            // getInstance() method is called with algorithm SHA-1 
            MessageDigest md = MessageDigest.getInstance("SHA-1"); 
  
            // digest() method is called 
            // to calculate message digest of the input string 
            // returned as array of byte 
            byte[] messageDigest = md.digest(plainText.getBytes()); 
  
            // Convert byte array into signum representation 
            BigInteger no = new BigInteger(1, messageDigest); 
  
            // Convert message digest into hex value 
            String hashtext = no.toString(16); 
  
            // Add preceding 0s to make it 32 bit 
            while (hashtext.length() < 32) { 
                hashtext = "0" + hashtext; 
            } 
  
            // return the HashText 
            return hashtext; 
        } 
  
        // For specifying wrong message digest algorithms 
        catch (NoSuchAlgorithmException e) { 
            return "error";
        } 
	}

	@Override
	public String decrypt(String cypherText, String key) 
	{
		return "hashes are one-way functions!";
	}

	@Override
	public String getRndKey() 
	{
		int keySize = 13;
		StringBuilder sb = new StringBuilder();
		Random rng = new Random();
		
		for(int i = 0; i < keySize; i += 1) 
		{
			sb.append((char)(rng.nextInt(127)+32));
		}
		return sb.toString();
	}

	@Override
	public String encrypt(String plainText) {
		// TODO Auto-generated method stub
		return null;
	}

	
	

}
