package encryptions;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.Random;

public class MD5 implements IFEncryption{

	@Override
	public String encrypt(String clearText, String key) {
		
		try { 
			  
            // Static getInstance method is called with hashing MD5 
            MessageDigest md = MessageDigest.getInstance("MD5"); 
  
            // digest() method is called to calculate message digest 
            //  of an input digest() return array of byte 
            byte[] messageDigest = md.digest((key+clearText).getBytes()); 
  
            // Convert byte array into signum representation 
            BigInteger no = new BigInteger(1, messageDigest); 
  
            // Convert message digest into hex value 
            String hashtext = no.toString(16); 
            while (hashtext.length() < 32) { 
                hashtext = "0" + hashtext; 
            } 
            return hashtext; 
        }  
  
        // For specifying wrong message digest algorithms 
        catch (Exception e) { 
            
            return "error";
        }
	}

	@Override
	public String decrypt(String cypherText, String key) {
		// TODO Auto-generated method stub
		return "hashes are one-way functions!";
	}

	@Override
	public String getRndKey() {
		int keySize = 13;
		
		StringBuilder sb = new StringBuilder();
		Random rng = new Random();
		
		for(int i = 0; i < keySize; i += 1) 
		{
			sb.append((char)(rng.nextInt(127)+32));
		}
		return sb.toString();
	}

	@Override
	public String encrypt(String plainText) {
		// TODO Auto-generated method stub
		return encrypt(plainText,null);
	}

	
}
