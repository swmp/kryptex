package encryptions;

/*
 * Interface für den Gebrauch der verschiedenen Verschluesselungs-/Hashingalgorithmen
 */

public interface IFEncryption {
	
	/*
	 * Verschluesselungsfunktion 
	 * ohne Schluessel fuer Hashfunktionen
	 * @param plainText der Klartext
	 * @return generierter Hash als String
	 */
	public String encrypt(String plainText);
	
	/*
	 * Verschluesselungsfunktion
	 * @param plainText der Klartext
	 * @param key der Schluessel / das Salt bei Hashfunktionen
	 * @return Cipher bzw. genereierter Hash als String
	 */
	public String encrypt(String plainText, String key);
	
	/*
	 * Entschluesselungsfunktion
	 * @param cypherText der verschluesselte Text
	 * @param key der Schluessel
	 * @return entscluesselter Text als String
	 */
	public String decrypt(String cypherText, String key);
	
	/*
	 * Random Key Generator
	 * @return für die Verschluesselung angepassten Zufallsschluessel
	 */
	public String getRndKey();
}
